import os, shutil

def nomFonctionCaml(ligne) :
    lstrip = ligne.strip()
    if lstrip.startswith("type") :
        return "type"
    elif lstrip.startswith("let") :
        nomfonc = lstrip[4:]
        if nomfonc.startswith("rec ") :
            nomfonc = nomfonc[4:]
        nomfonc = nomfonc.split()[0]
        return nomfonc.split("=")[0]
    else :
        return None

def nomFonctionPython(ligne) :
    lstrip = ligne.strip()
    if lstrip.startswith("def") :
        nomfonc = lstrip[4:]
        return nomfonc.split("(")[0].strip()
    else :
        return None

nomFonction = {"python" : nomFonctionPython, "caml" : nomFonctionCaml}
balise = {"python" : "### {} ###", "caml" : "(*** {} ***)"}

def extraireCode(nf_tex, mlref, lang) :
    nf_tex = nf_tex.replace("file://", "")
    nf_tex_bak = nf_tex + ".bak"
    shutil.copy2(nf_tex, nf_tex_bak)
    nf_ml = os.path.join(os.path.dirname(nf_tex), mlref)
    contenu = open(nf_tex).readlines()
    output_tex = open(nf_tex, "w")
    output_ml = open(nf_ml, "w")
    incode = False
    nomfonc = ""
    complement = None
    compteursNoms = {}
    def setNom(s) :
        if s not in compteursNoms :
            compteursNoms[s] = 0
            return s
        else :
            compteursNoms[s] += 1
            return "{}__{}".format(s, compteursNoms[s])
    try :
        for l in contenu :
            lstrip = l.strip()
            if lstrip.startswith(r"\begin{lst"+lang+"}") :
                incode = True
                nomfonc = ""
                complement = None
                if "[" in lstrip :
                    complement = lstrip.split("[")[1].split("]")[0]
                continue
            if incode :
                if nomfonc == "" :
                    if lstrip == "" :
                        continue
                    nomfonc = nomFonction[lang](lstrip)
                    if nomfonc is None :
                        nomfonc = "anon"
                    nomfonc = setNom(nomfonc)
                    output_ml.write(balise[lang].format(nomfonc) + "\n")
                if lstrip == r"\end{lst"+lang+"}" :
                    output_ml.write(balise[lang].format("/") + "\n\n")
                    if complement is not None :
                        output_tex.write(r"\input" + lang + "["+nomfonc+"]{"+ mlref + "}["+complement+"]\n")
                    else :
                        output_tex.write(r"\input" + lang + "["+nomfonc+"]{"+ mlref + "}\n")
                    incode = False
                    continue
                output_ml.write(l)
            else :
                output_tex.write(l)
    finally :
        output_ml.close()
        output_tex.close()