\NeedsTeXFormat {LaTeX2e}
\ProvidesClass {chato-exo2}[2023-12-31 v0.9]

% l3keys2e fait partie de LaTeX base depuis juin 2022
% l3keys2e charge expl3
\@ifl@t@r\fmtversion{2022-06-01}{}{\RequirePackage{l3keys2e, xparse}}
\ExplSyntaxOn

\sys_if_engine_luatex:F
 {\sys_if_engine_xetex:F
  {\PackageError{chato-exo}{Ne~fonctionne~qu'avec~XeLaTeX~ou~LuaLaTeX}{}}}

% mode de compilation : enonce, solution, complet = les deux
% C'est une token-list plutôt qu'une string parce que sinon la comparaison des modes
% avec \clist_if_in ne fonctionne pas (https://tex.stackexchange.com/q/629998).
% On définira \g__chato_exo_mode_str quand on aura la valeur définitive.
\tl_new:N \g__chato_exo_mode_tl

% permet de charger le paquet avec [mode=enonce] ou solution ou complet
% et de définir les éléments de l'en-tête : niveau, nature, titre, etablissement
\str_new:N \g__chato_exo_reinit_question_str
\keys_define:nn { chato-exo2 }
  {
     mode   .choices:nn = {enonce, solution, complet, semicomplet} {\tl_gset:Nx \g__chato_exo_mode_tl {\tl_use:N \l_keys_choice_tl}},
    niveau .tl_gset:N = \g__chato_niveau_tl,
    nature .tl_gset:N = \g__chato_nature_tl,
    titre  .tl_gset:N = \g__chato_titre_tl,
    etab   .tl_gset:N = \g__chato_etab_tl,
    mode    .value_required:n = {true},
    niveau  .value_required:n = {true},
    nature  .value_required:n = {true},
    titre   .value_required:n = {true},
    etab    .tl_gset:N = \g__chato_etab_tl,
    etab   .initial:n = {Lycée~Chateaubriand},
    lang    .tl_gset:N = \g__chato_lang_tl,
    lang   .initial:n = {french},
    numquest .value_required:n = {true},
    numquest .str_gset:N = \g__chato_exo_reinit_question_str,
    unknown .code:n           =
     \PackageWarning{chato-exo}{Unknown~ option~ `\l_keys_key_str'}
  }

% \ProcessKeyOptions fait partie de LaTeX base depuis juin 2022.
% Par ailleurs on n'a pas avec lui de problème d'espaces,
% donc plus besoin de {} sur le titre/nature,
% donc plus besoin du hack ci-après.
\@ifl@t@r\fmtversion{2022-06-01}
  {\ProcessKeyOptions}
  {\ProcessKeysOptions { chato-exo2 }}

% On supprime la liste des options de la classe car si babel la reçoit,
% il va déclencher une erreur en raison de la présence de {} dans notre nature ou titre.
% cf https://tex.stackexchange.com/questions/479552
\clist_clear:N \@classoptionslist

% permet de changer le mode selon que jobname contient -ENONCE, -SOLUTION ou -COMPLET
 \str_if_in:NnTF {\c_sys_jobname_str} {-ENONCE} {\tl_gset:Nn \g__chato_exo_mode_tl {enonce}}
{\str_if_in:NnTF {\c_sys_jobname_str} {-SOLUTION} {\tl_gset:Nn \g__chato_exo_mode_tl {solution}}
{\str_if_in:NnTF  {\c_sys_jobname_str} {-COMPLET} {\tl_gset:Nn \g__chato_exo_mode_tl {complet}}
{\str_if_in:NnT {\c_sys_jobname_str} {-SEMI} {\tl_gset:Nn \g__chato_exo_mode_tl {semicomplet}}
}}}

\str_const:Nx \g__chato_exo_mode_str {\tl_to_str:N \g__chato_exo_mode_tl}

\LoadClass[10pt, a4paper, \g__chato_lang_tl]{article}
\RequirePackage[\g__chato_lang_tl]{babel}
\exp_args:No \str_case:nn {\tl_to_str:N \g__chato_lang_tl}
{
  {french}
    {\frenchbsetup{og=«, fg=»}}
}
\RequirePackage{csquotes}
\RequirePackage[citestyle=alphabetic,bibstyle=alphabetic]{biblatex}
\RequirePackage{amsmath,stmaryrd,amssymb,fancybox,fancyhdr,lastpage,mdwlist}

\RequirePackage{bookmark} % charge aussi hyperref
\hypersetup{hidelinks}

% contiendra COMPLET ou SOLUTION dans le mode idoine
\str_new:N \g__chato_exo_mode_titre_str

%%% GESTION DES LABELS EN MODE SOLUTION
% Plutôt que d'ignorer complètement le contenu des questions, on va le typographier dans
% une vbox qu'on va jeter ensuite. L'intérêt est que si des labels sont définis,
% par exemple pour des équations, on pourra y faire référence quand même.
% Pour que ça marche, il faut extraire les labels de la boite.
% Pour cela, on modifie localement \label
% pour qu'il mette les informations voulues dans une liste \g__chato_exo_labels_cl, qu'on 
% traite après avoir refermé la boite en appliquant sur la liste une fonction qui imite ce que 
% fait originellement \label (adaptation de https://tex.stackexchange.com/a/273480).

% Selon que amsmath ou hyperref sont chargés, il y des modifs :
%   - manipuler \ltx@label au lieu de \label (amsmath)
%   - stocker et utiliser plus d'informations pour chaque label (hyperref)
% Il faut donc charger ces paquets avant, pour que le comportement approprié puisse être pris.

% Liste des label au sein de l'environnement
\clist_new:N \g__chato_exo_labels_cl
% Boite poubelle pour typographier l'environnement
\box_new:N \l__chato_exo_dummy_box

  
% Si hyperref est chargé, il y a plus d'informations à stocker et ressortir
% \cs__chato_exo_qlabel:n sera le \label ou \ltx@label dans l'environnement : 
%     il ajoute les infos de label (3 ou 5) dans la liste.
% \cs__chato_exo_dolabels prend 3 ou 5 paramètres et fait ce que ferait \label ordinairement.
\@ifpackageloaded{hyperref}
  {
    \cs_new:Npn\cs__chato_exo_dolabels#1#2#3#4#5{\@bsphack \begingroup \def \label@name {#1}\label@hook \protected@write \@auxout {}{\string \newlabel {#1}{{#2}{#3}{#4}{#5}{}}}\endgroup \@esphack}
    \cs_new:Nn\cs__chato_exo_qlabel:n{\clist_gput_right:Nx \g__chato_exo_labels_cl { {{#1}{\@currentlabel}{\thepage}{\@currentlabelname}{\@currentHref}} } }
  }
  {
    \cs_new:Npn\cs__chato_exo_dolabels#1#2#3{\@bsphack \protected@write \@auxout {}{\string \newlabel {#1}{{#2}{#3}}}\@esphack}
    \cs_new:Nn\cs__chato_exo_qlabel:n{\clist_gput_right:Nx \g__chato_exo_labels_cl { {{#1}{\@currentlabel}{\thepage}} } }
  }

% Pour savoir si on est en train de cacher un environnement, on compte combien sont en cours.
% L'idée est de ne pas cacher dans quelque chose de caché, car ça mettrait le bazar
% avec la liste des labels.
\int_new:N \g__chato_exo_silent_en_cours_int

% Ceci est à mettre au début de l'environnement qu'on veut masquer
\tl_const:Nn \g__chato_exo_begin_silent_env_tl
  {\int_compare:nNnT{\g__chato_exo_silent_en_cours_int}={0}{
    \group_begin:
    \cs_set_eq:NN\label\cs__chato_exo_qlabel:n
    %% Si amsmath est chargé, tout environnement comme equation redéfinit label pour faire des trucs, et ensuite appeler \ltx@label : on doit donc aussi remplacer cette commande.
    %% https://tex.stackexchange.com/a/655523
    \@ifpackageloaded{amsmath}{\cs_set_eq:NN\ltx@label\cs__chato_exo_qlabel:n}{}
    %% Cette commande est appelée quand on démarre un figure notamment.
    %% Cette définition, inspirée du mode H du paquet float, désactive tous les traitements spécifiques des floats mais définit \@captype afin que le caption soit correctement géré.
    \cs_set_nopar:Npn\@xfloat #1[#2]{\cs_set_eq:cN {end#1}\relax\cs_set_nopar:Npn \@captype {#1}}
     \vbox_set:Nw \l__chato_exo_dummy_box
   }
   \int_gincr:N\g__chato_exo_silent_en_cours_int}

% Ceci est à mettre à la fin   
\tl_const:Nn \g__chato_exo_end_silent_env_tl
  {\int_gdecr:N\g__chato_exo_silent_en_cours_int
   \int_compare:nNnT{\g__chato_exo_silent_en_cours_int}={0}{
     \vbox_set_end:\group_end:
     \clist_map_inline:Nn \g__chato_exo_labels_cl {\cs__chato_exo_dolabels #1}
     \clist_gclear:N \g__chato_exo_labels_cl
   }}

%%% FIN GESTION DES LABELS EN MODE SOLUTION

%%% AFFICHAGE DES QUESTIONS ET SOLUTIONS
\RequirePackage{enumitem}
\RequirePackage{chato-comment}

% Définition du compteur de questions, en lien avec l'option de réinitialisation.
% \counterwithin est mieux que \newcounter{question}[\g__chato_exo_reinit_question_str]
% car il modifie aussi \thequestion pour y faire apparaitre le numéro de (sous-)section.
\newcounter{question}
\str_case:VnF \g__chato_exo_reinit_question_str
{
  {} {}
  {document} {}
}
{
  \counterwithin{question}{\g__chato_exo_reinit_question_str}
}

% La décalage horizontal des questions (sert à augmenter \leftskip).
\dim_const:Nn \g__chato_exo_question_leftskip {1.5em}

% Un booléen interne pour savoir si on est au début d'une question (sert à remonter un ssquest quand il arrive dès le début, de sorte à éviter une ligne blanche). Il est positionné par \cs__chato_exo_question:nnnnn et lu par le code qui gère ssquest.
\bool_new:N \g__chato_exo_question_debut

% Sert à typographier le numéro de question avec les symboles en dessous, sans que ceux-ci occupent de place verticale.
\cs_new:Nn \cs__chato_exo_question_symbs:nn {
  \hbox_set:Nn \l_tmpa_box { #1 }
  \vbox_to_ht:nn { \dim_eval:n { \box_ht:N \l_tmpa_box + \box_dp:N \l_tmpa_box } }
   {
    \hbox_to_zero:n { \hss #1 }
    \offinterlineskip % Turn off interline skip
    \clist_map_inline:nn { #2 }
     {
      \vskip1mm
      \hbox_to_zero:n {\hss \hbox_to_wd:nn {1em}{ \hss ##1 \hss}}
     }
   }
 }
 
\NewDocumentCommand{\ParCoeur}{O{1}}{$\heartsuit\int_step_inline:nn{#1 -1}{\mkern-7mu\heartsuit}$}
\NewDocumentCommand{\Difficile}{O{1}}{$\int_step_inline:nn{#1}{\star}$}
\NewDocumentCommand{\Maison}{}{{\scriptsize\faIcon{home}}}


%% sera réglé par \cs__chato_exo_question ; doit être déclaré ici pour que la première question compile en mode solution.
\tl_new:N \g__chato_exo_question_target_tl
% Invoqué au début de chaque question;
\cs_new:Nn \cs__chato_exo_question:nnnnnn{
  % #1 : type de question (compteur)
  % #2 : "numero" de question (si pas de compteur)
  % #3 : titre éventuel
  % #4 : complément éventuel (symboles) ; est vide s'il n'y en a pas
  % #5 : skip avant
  % #6 : ancre hyperref
  \hook_gclear_next_code:n{para/before}
  #5\par\noindent
  \tl_if_novalue:nF {#1} {\refstepcounter{#1}}
  \group_begin:
  \dim_add:Nn \leftskip \g__chato_exo_question_leftskip
  \nopagebreak
   % bookmark
  \tl_if_novalue:nTF {#6}
    {
      \tl_clear:N \g__chato_exo_question_target_tl
    }
    {
      \Hy@raisedlink{\hypertarget{#6}{}}\bookmark[dest={#6}, rellevel=1, keeplevel]{#6}
      \tl_set:Nn \g__chato_exo_question_target_tl {#6}
    }
  % Le numéro de question et ses symboles. Si un "numéro" a été donné, on l'utilise, sinon c'est la valeur du compteur (erreur si on n'a ni l'un ni l'autre).
  \makebox[0pt][r]{
  % affichage proprement dit
    \cs__chato_exo_question_symbs:nn{\textbf{\tl_if_novalue:nTF {#2} {\@nameuse{the#1}} {#2}.}}{#4} \hspace*{.25em}
  }
  %
  % Le titre éventuel.
  \tl_if_novalue:nTF {#3}
    % Quand il est absent, on note qu'on est au début de la question, et on met en place un hook qui va retirer ce booléen à la fin du paragraphe.  
    {\bool_gset_true:N \g__chato_exo_question_debut \hook_gput_next_code:nn{para/end}{\bool_gset_false:N \g__chato_exo_question_debut}
    \hspace*{\parindent}\null}
  %  
    % S'il y a un titre, on va à la ligne après et on n'est donc pas au début de la question.
    {\textsf{#3} \nopagebreak\par\nopagebreak\bool_gset_false:N \g__chato_exo_question_debut}
}

% Nécessaire car pour passer NoValue en dur, il faut en demander l'expansion pour que \tl_if_novalue marche.
\cs_generate_variant:Nn \cs__chato_exo_question:nnnnnn  {nonnnn, onnnnn, nonnno, onnnno}

% À la fin d'une question
\cs_new:Nn \cs__chato_exo_endquestion:{
  \par\group_end:
  \hook_gput_next_code:nn{para/before}{\addvspace{\dim_eval:n{3\smallskipamount}}}
  \hook_gput_next_code:nn{cmd/section/before}{\hook_gclear_next_code:n{para/before}}
  \hook_gput_next_code:nn{cmd/subsection/before}{\hook_gclear_next_code:n{para/before}}
  \hook_gput_next_code:nn{cmd/subsubsection/before}{\hook_gclear_next_code:n{para/before}}
  \hook_gput_next_code:nn{cmd/paragraph/before}{\hook_gclear_next_code:n{para/before}}
  \ignorespacesafterend
}


%% réglages communs aux modes complet et semicomplet
\tl_const:Nn \g__chato_exo_complet_env_tl {

 \NewDocumentEnvironment{question}{D(){}o}{
        \cs__chato_exo_question:nonnnn{question}{\c_novalue_tl}{#2}{#1}{\addvspace{\medskipamount}\pagebreak[0]}{question-\thequestion}
        \ignorespaces
      }
      {
        \cs__chato_exo_endquestion:

      }
      
      \NewDocumentEnvironment{test}{D(){}o}{
              \cs__chato_exo_question:onnnnn{\c_novalue_tl}{Test}{#2}{#1}{}{test-\thequestion}
              \ignorespaces
            }
            {
              \cs__chato_exo_endquestion:
      
            }
            
      \newcounter{questionPTP}
      \NewDocumentEnvironment{questionPTP}{D(){}o}{
        \cs__chato_exo_question:nonnno{questionPTP}{\c_novalue_tl}{#2}{#1}{\addvspace{\medskipamount}\pagebreak[0]}{\c_novalue_tl}
        \ignorespaces
      }
      {
        \cs__chato_exo_endquestion:

      }
      
      \NewDocumentEnvironment{testPTP}{D(){}o}{
              \cs__chato_exo_question:onnnno{\c_novalue_tl}{Test}{#2}{#1}{}{\c_novalue_tl}
              \ignorespaces
            }
            {
              \cs__chato_exo_endquestion:
      
            }
      
      \NewDocumentEnvironment{solution}{D(){}o}{
              \cs__chato_exo_question:onnnnn{\c_novalue_tl}{Solution}{#2}{#1}{\pagebreak[0]}{solution-\thequestion}
              \ignorespaces
            }
            {
              \cs__chato_exo_endquestion:
      
            }
}

% Ne fonctionne pas chez Amélie…
%\str_case:VnF \g__chato_exo_mode_str
\exp_args:No \str_case:nnF {\g__chato_exo_mode_str}
  { % debut case
    {enonce}{
      % \str_log:n n'existe pas chez Amélie
      %\str_log:n {chato-exo~:~Mode~énoncé}
      
      \NewDocumentEnvironment{question}{D(){}!o}{
        \cs__chato_exo_question:nonnnn{question}{\c_novalue_tl}{#2}{#1}{\addvspace{\medskipamount}\pagebreak[0]}{question-\thequestion}
        \ignorespaces
      }
      {
        \cs__chato_exo_endquestion:

      }
      
      \NewDocumentEnvironment{test}{D(){}!o}{
        \cs__chato_exo_question:onnnnn{\c_novalue_tl}{Test}{#2}{#1}{}{test-\thequestion}
        \ignorespaces
      }
      {
        \cs__chato_exo_endquestion:

      }
      
      \managecomment{solution}
      \managecomment{questionPTP}
      \managecomment{testPTP}
    }
    {solution}{
      %\str_log:n {chato-exo~:~Mode~solution}
      \str_set:Nn \g__chato_exo_mode_titre_str {SOLUTION}

      % Ceci permet de compter les questions, alors même qu'on ne les affiche pas.
      % Ce compteur sera celui utilisé pour produire le numéro de la solution.
      % C'est ce dispositif qui permet une numérotation correcte des solutions lorsque 
      % certaines questions ne sont pas corrigées.
      \NewDocumentEnvironment{question}{D(){}o}
      {\refstepcounter{question}
       \g__chato_exo_begin_silent_env_tl}
      {\g__chato_exo_end_silent_env_tl}
      
      \NewDocumentEnvironment{test}{O{}}
      {\g__chato_exo_begin_silent_env_tl}
      {\g__chato_exo_end_silent_env_tl}
      
      \NewDocumentEnvironment{solution}{D(){}o}{
        \cs__chato_exo_question:onnnnn{\c_novalue_tl}{\thequestion}{#2}{#1}{\addvspace{\medskipamount}\pagebreak[0]}{solution-\thequestion}
        \ignorespaces
      }
      {
        \cs__chato_exo_endquestion:

      }
      
      \newcounter{questionPTP}
      \NewDocumentEnvironment{questionPTP}{D(){}o}{
        \cs__chato_exo_question:nonnno{questionPTP}{\c_novalue_tl}{#2}{#1}{\addvspace{\medskipamount}\pagebreak[0]}{\c_novalue_tl}
        \ignorespaces
      }
      {
        \cs__chato_exo_endquestion:

      }
                
      \NewDocumentEnvironment{testPTP}{D(){}o}{
        \cs__chato_exo_question:onnnno{\c_novalue_tl}{Test}{#2}{#1}{}{\c_novalue_tl}
        \ignorespaces
      }
      {
        \cs__chato_exo_endquestion:

      }
    }
    {complet}{
      %\str_log:n {chato-exo~:~Mode~complet}
      \str_set:Nn \g__chato_exo_mode_titre_str {COMPLET}
      \g__chato_exo_complet_env_tl
     
    }
    {semicomplet}{
      %\str_log:n {chato-exo~:~Mode~semicomplet}
      \str_set:Nn \g__chato_exo_mode_titre_str {SEMICOMPLET}
      \g__chato_exo_complet_env_tl
    }
  } % fin case
  { %% aucun cas trouvé
    \PackageError{chato-exo}{Mode~non~spécifié~ou~invalide~:~\g__chato_exo_mode_str}{Vous~devez~charger~le~paquet~avec~mode=enonce~ou~solution~ou~complet,~ou~bien~utiliser~un~jobname~qui~contient~-ENONCE,~-SOLUTION~ou~-COMPLET.}
  }

\newlist{ssquest}{enumerate}{1}
\setlist[ssquest]{
  label={(\alph*)}, ref={\thequestion.(\alph*)},
  topsep=2pt,
  
  % Gestion de l'alignement horizontal
  leftmargin=\dim_eval:n{\leftskip+\labelwidth+\labelsep},
  
  % Éviter une ligne blanche si on n'a rien avant dans la question.
  % Le booléen sert à savoir si on est dans le premier paragraphe, la condition sur \lastskip est une astuce pour savoir si on est au début du paragraphe courant ; elle s'appuie sur le fait qu'après un mot, LaTeX met de la glue qui n'est pas 0pt.
  before={%
  \bool_if:nT {\g__chato_exo_question_debut && \dim_compare_p:nNn {\lastskip}={0pt}}%
              {\vspace*{\dim_eval:n{-\topsep-\baselineskip-\parskip}}}\hspace*{-\parindent}%
  },
  
  % ajouter des bookmarks s'il y en avait un pour la question
  format={%
  \tl_if_empty:NF {\g__chato_exo_question_target_tl}%
    {\Hy@raisedlink{\hypertarget{\g__chato_exo_question_target_tl-\alph{\@enumctr}}{}}\bookmark[dest={\g__chato_exo_question_target_tl-\alph{\@enumctr}}, rellevel=2, keeplevel]{\g__chato_exo_question_target_tl-\alph{\@enumctr}}%
    }%
  }
}

%%% FIN AFFICHAGE DES QUESTIONS ET SOLUTIONS


%%% GESTION DES ENVIRONNEMENTS À MODE
% Détecte si le mode en cours fait partie de la liste de modes passée en paramètre (convention : oui si la liste est vide).
\prg_new_conditional:Nnn \if__chato_exo_mode_active:n {T,F,TF} {
  \clist_if_empty:nTF {#1}
    {\prg_return_true:}
    {\clist_if_in:nVTF {#1} \g__chato_exo_mode_tl
      {\prg_return_true:}
      {\prg_return_false:}}
}

\NewDocumentEnvironment{onlyenv}{D<>{}}
  {\if__chato_exo_mode_active:nF{#1}{\g__chato_exo_begin_silent_env_tl}}
  {\if__chato_exo_mode_active:nF{#1}{\g__chato_exo_end_silent_env_tl}}

\NewDocumentEnvironment{remarque}{D<>{}}
  {\if__chato_exo_mode_active:nTF{#1}
    {\par\textbf{Remarque~:}}
    {\g__chato_exo_begin_silent_env_tl}
  }
  {\if__chato_exo_mode_active:nF{#1}{\g__chato_exo_end_silent_env_tl}}
  
\RequirePackage{multicol}
\NewDocumentEnvironment{colonnes}{mD<>{}}
  {\if__chato_exo_mode_active:nT{#2}{\begin{multicols}{#1}}}
  {\if__chato_exo_mode_active:nT{#2}{\end{multicols}}}

\NewDocumentCommand{\colonnebreak}{D<>{}}{
  \if__chato_exo_mode_active:nT{#1}{\columnbreak}
}

\NewCommandCopy\chato__exo_pagebreak\pagebreak
\RenewDocumentCommand{\pagebreak}{O{4}D<>{}}{
  \if__chato_exo_mode_active:nT{#2}{\chato__exo_pagebreak[#1]}
}
\RequirePackage{mdframed}
\NewDocumentCommand{\cadreReponse}{O{4cm}}{
  \if__chato_exo_mode_active:nT {enonce}
  {\begin{mdframed}[linewidth=1pt]\vspace{#1}\end{mdframed}}}

%%% FIN GESTION DES ENVIRONNEMENTS À MODE

\RequirePackage{geometry}
\geometry{a4paper, top=1cm, includehead, head=0.53cm, bottom=1cm, includefoot, left=1.5cm, right=1.5cm}
\NewDocumentCommand{\Paysage}{D<>{}}{
\if__chato_exo_mode_active:nT {#1} {\geometry{landscape}}
}

\RequirePackage{chato-listing, chato-notations, chato-tikz}


\AtBeginDocument{
\pagestyle{fancy}

\lhead{{{\textit{\g__chato_nature_tl}}}~---~{{\g__chato_niveau_tl}}}
\chead{\g__chato_exo_mode_titre_str}
\rhead{\bfseries{\large \g__chato_titre_tl}}
\cfoot{}
\lfoot{\g__chato_etab_tl}
\rfoot{\thepage/\pageref{LastPage}}

\renewcommand{\footrulewidth}{0.4pt}
}

\ExplSyntaxOff
