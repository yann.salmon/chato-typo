\NeedsTeXFormat {LaTeX2e}
\ProvidesClass {chato-beamer}[2023-08-05 0.6]

%% Gère la compilation de présentations beamer et d'articles basés sur beamerarticle.
%% Ajoute au comportement de beamer la détection du mode dans le jobname, ce qui permet d'automatiser la génération des différentes version avec un script.
%% Ajoute aussi quelques fonctionnalités, notamment en lien avec TikZ.

% À partir de la version de juin 2022, expl3 fait partie de la base de LaTeX
\@ifl@t@r\fmtversion{2022-06-01}{}{\RequirePackage{expl3}}
\ExplSyntaxOn

\NewDocumentCommand{\nature}{m}{\str_const:Nn \g__chato_beamer_nature {#1}}
\NewDocumentCommand{\niveau}{m}{\str_const:Nn \g__chato_beamer_niveau {#1}}
\AtBeginDocument{
 \str_new:N \g__chato_beamer_natniv
  \str_if_exist:NT \g__chato_beamer_nature {
    \str_gset_eq:NN \g__chato_beamer_natniv \g__chato_beamer_nature
    \str_if_exist:NT \g__chato_beamer_niveau {
      \str_gput_right:Nn \g__chato_beamer_natniv {~---~}
    }
  }
  \str_if_exist:NT \g__chato_beamer_niveau {
     \str_gput_right:NV \g__chato_beamer_natniv \g__chato_beamer_niveau
  }
}

% Ceci pour neutraliser une option de mode mise dans le fichier source lorsque jobname détermine ce mode
\tl_const:Nn \g__chato_beamer_ignore_mode_tl
  {\DeclareOption{beamer}{}\DeclareOption{handout}{}\DeclareOption{trans}{}\DeclareOption{article}{}\DeclareOption{complet}{}}
  
\tl_const:Nn \g__chato_beamer_ignore_aspectratio_tl
  {\DeclareOption{aspectratio=169}{}\DeclareOption{aspectratio=1610}{}\DeclareOption{aspectratio=43}{}}
  
%% Cette option ne fait rien, mais son existence autorise à écrire avecarticle dans les crochets quand on charge la classe, ce qui peut être détecté par le script maketd pour compiler avec -ARTICLE au lieu de -HANDOUT.
\DeclareOption{avecarticle}{}

\str_new:N \g__chato_beamer_slides_per_page
\str_new:N \g__chato_beamer_orientation
\bool_new:N \g__chato_beamer_article_mode
\bool_new:N \g__chato_beamer_complet_mode

\tl_new:N \g__chato_beamer_lang
\tl_gset:Nn \g__chato_beamer_lang {french}

\DeclareOption{english}{\tl_gset:Nn \g__chato_beamer_lang {english}}

\tl_const:Nn \g__chato_beamer_two_on_one_tl
  {\str_gset:Nn \g__chato_beamer_slides_per_page {2~on~1}}
  
\tl_const:Nn \g__chato_beamer_four_on_one_tl
  {\str_gset:Nn \g__chato_beamer_slides_per_page {4~on~1}
  \str_gset:Nn \g__chato_beamer_orientation {landscape}}
  
\tl_const:Nn \g__chato_beamer_ignore_disposition_tl
  {\DeclareOption{4on1}{}\DeclareOption{2on1}{}}

% permet de changer le mode selon que jobname contient -BEAMER, etc.
\str_if_in:NnTF {\c_sys_jobname_str} {-BEAMER} % then
{
 \PassOptionsToClass{beamer}{beamer}
 \g__chato_beamer_ignore_mode_tl
}
% else
{
  \str_if_in:NnTF {\c_sys_jobname_str} {-TRANS} % then
  {
   \PassOptionsToClass{trans}{beamer}
   \g__chato_beamer_ignore_mode_tl
  }
  % else
  {
    \str_if_in:NnTF  {\c_sys_jobname_str} {-HANDOUT} % then
    {
      \PassOptionsToClass{handout}{beamer}
      \g__chato_beamer_ignore_mode_tl
    }
    {
      \str_if_in:NnTF {\c_sys_jobname_str} {-ARTICLE} % then
      {
        \bool_gset_true:N \g__chato_beamer_article_mode
        \g__chato_beamer_ignore_mode_tl
      }
      {
        \str_if_in:NnTF {\c_sys_jobname_str} {-COMPLET}
        {
          \bool_gset_true:N \g__chato_beamer_article_mode \bool_gset_true:N \g__chato_beamer_complet_mode
          \g__chato_beamer_ignore_mode_tl  
        }
        {
          \DeclareOption{article}{\bool_gset_true:N \g__chato_beamer_article_mode}
          \DeclareOption{complet}{\bool_gset_true:N \g__chato_beamer_article_mode \bool_gset_true:N \g__chato_beamer_complet_mode}
        } % fin else COMPLET
      } % fin else ARTICLE
    } % fin else HANDOUT
  } % fin else TRANS
} % fin else BEAMER

\str_if_in:NnTF {\c_sys_jobname_str} {-169} % then
{
  \PassOptionsToClass{aspectratio=169}{beamer}
  \g__chato_beamer_ignore_aspectratio_tl
}
% else
{
  \str_if_in:NnTF {\c_sys_jobname_str} {-1610} % then
  {
   \PassOptionsToClass{aspectratio=1610}{beamer}
   \g__chato_beamer_ignore_aspectratio_tl
  }
  % else
  {
    \str_if_in:NnT {\c_sys_jobname_str} {-43} % then
    {
       \PassOptionsToClass{aspectratio=43}{beamer}
       \g__chato_beamer_ignore_aspectratio_tl
    }
  }
}

\str_if_in:NnTF \c_sys_jobname_str {-2ON1} % then
{
  \g__chato_beamer_two_on_one_tl \g__chato_beamer_ignore_disposition_tl
}
% else
{
  \str_if_in:NnTF \c_sys_jobname_str {-4ON1} % then
  {
    \g__chato_beamer_four_on_one_tl \g__chato_beamer_ignore_disposition_tl
  }
  % else
  {
    \str_if_in:NnTF \c_sys_jobname_str {-1ON1} % then
    {
      \g__chato_beamer_ignore_disposition_tl
    }
    % else
    {
      \DeclareOption{2on1}{\g__chato_beamer_two_on_one_tl}
      \DeclareOption{4on1}{\g__chato_beamer_four_on_one_tl}
    }
  }
}


\PassOptionsToClass{\g__chato_beamer_lang, xcolor, 10pt, notheorems}{beamer}
\PassOptionsToClass{margedroite}{articleYS}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{beamer}\PassOptionsToClass{\CurrentOption}{articleYS}}

\ProcessOptions

\bool_if:nTF {\g__chato_beamer_article_mode}
{\LoadClass{articleYS}\RequirePackage[noamsthm]{beamerarticle}}
{\LoadClass{beamer}}

\mode
<presentation>
\str_if_eq:VnTF\insertaspectratio{43}{\NewDocumentCommand\onlywide{+m}{}}{\NewDocumentCommand\onlywide{+m}{#1}}

\mode
<article>
\NewDocumentCommand\onlywide{+m}{#1}

\mode
<handout|trans>
\str_if_empty:NF \g__chato_beamer_slides_per_page
{
  \RequirePackage{pgfpages}
  %% https://tex.stackexchange.com/a/657901
  \use:e{\exp_not:N\pgfpagesuselayout{\g__chato_beamer_slides_per_page}[a4paper,\g__chato_beamer_orientation,border~shrink=5mm]}
}
\mode
<all>


\RequirePackage[\g__chato_beamer_lang]{babel}

\str_case:Vn \g__chato_beamer_lang
{
  {french}
    {\frenchbsetup{og=«, fg=»}}
}

\RequirePackage{csquotes}
\RequirePackage[citestyle=alphabetic,bibstyle=alphabetic]{biblatex}


%% La commande \placehold<> permet de cacher du texte au début d'une frame beamer (comme uncover), mais en laissant un trou dans la version article (que les élèves seront censés compléter). En effet, la commande uncover de beamer affiche les éléments masqués dans la version article, et ne convient donc pas pour cela.


\mode
<article|handout>

%% depuis beamerbaseoverlay.sty
%\newskip\beamer@lastskipcover
%\def\beamer@smuggle#1{%
%  {}%
%  \beamer@lastskipcover=\lastskip
%  \edef\beamer@lastskiptext{\the\lastskip}%
%  \ifx\beamer@lastskiptext\beamer@zeropt
%  \else
%    \ifvmode\unskip\fi
%    \ifhmode\unskip\fi
%  \fi
%  #1%
%  \ifx\beamer@lastskiptext\beamer@zeropt
%  \else
%    \ifvmode\vskip\beamer@lastskipcover\fi
%    \ifhmode\hskip\beamer@lastskipcover\fi
%  \fi
%}
%
%\def\beamer@@spacingcover{\beamer@smuggle{\pgfsys@begininvisible\pgfsys@endinvisible}}
%\def\beamer@spacingcover{\beamer@@spacingcover\aftergroup\beamer@@spacingcover}
%
%\def\beamer@begininvisible{\beamer@smuggle{\pgfsys@begininvisible\aftergroup\beamer@@spacingcover}}
%\def\beamer@endinvisible{\beamer@smuggle{\pgfsys@endinvisible\aftergroup\beamer@@spacingcover}}
%%%

\bool_if:nTF {\g__chato_beamer_complet_mode}
  {
      \newrobustcmd{\placehold}{\alt{}{}}
      \NewDocumentEnvironment{placeholdenv}{D<>{.-}}{}{}
    }
  {
    \long\def\ysbeamer@reallymakeinvisible#1{\pgfsys@begininvisible #1\pgfsys@endinvisible}
    \newrobustcmd{\placehold}{\alt{\ysbeamer@reallymakeinvisible}{\ysbeamer@reallymakeinvisible}}
    \NewDocumentEnvironment{placeholdenv}{D<>{.-}}{\pgfsys@begininvisible}{\pgfsys@endinvisible}
  }



\mode
<beamer|trans|second>
\newrobustcmd{\placehold}{\alt{\beamer@fakeinvisible}{\beamer@reallymakeinvisible}}
\NewDocumentEnvironment{placeholdenv}{D<>{.-}}{\begin{altenv}<#1>{}{}{\pgfsys@begininvisible}{\pgfsys@endinvisible}}{\end{altenv}}


\mode
<all>
\ExplSyntaxOff

\NewDocumentCommand{\phold}{}{\placehold<+(1)->}
\NewDocumentCommand{\ponly}{}{\only<+(1)->}
\NewDocumentCommand{\puncover}{}{\uncover<+(1)->}

%% Pour un \alt qui gère bien la largeur différente des versions
%% https://tex.stackexchange.com/a/63559/103608

\RequirePackage{etoolbox, mathtools}

% Detect mode. mathpalette is used to detect the used math style
\newcommand<>\Alt[3][l]{%
  \begingroup
    \providetoggle{Alt@before}%
    \alt#4{\toggletrue{Alt@before}}{\togglefalse{Alt@before}}%
    \ifbool{mmode}{%
      \expandafter\mathpalette
      \expandafter\math@Alt
    }{%
      \expandafter\make@Alt
    }%
    {{#1}{#2}{#3}}%
  \endgroup
}

% Un-brace the second argument (required because \mathpalette reads the three arguments as one
\newcommand\math@Alt[2]{\math@@Alt{#1}#2}

% Set the two arguments in boxes. The math style is given by #1. \m@th sets \mathsurround to 0.
\newcommand\math@@Alt[4]{%
  \setbox\z@ \hbox{$\m@th #1{#3}$}%
  \setbox\@ne\hbox{$\m@th #1{#4}$}%
  \@Alt{#2}%
}

% Un-brace the argument
\newcommand\make@Alt[1]{\make@@Alt#1}

% Set the two arguments into normal boxes
\newcommand\make@@Alt[3]{%
  \sbox\z@ {#2}%
  \sbox\@ne{#3}%
  \@Alt{#1}%
}

% Place one of the two boxes using \rlap and place a \phantom box with the maximum of the two boxes
\newcommand\@Alt[1]{%
  \setbox\tw@\null
  \ht\tw@\ifnum\ht\z@>\ht\@ne\ht\z@\else\ht\@ne\fi
  \dp\tw@\ifnum\dp\z@>\dp\@ne\dp\z@\else\dp\@ne\fi
  \wd\tw@\ifnum\wd\z@>\wd\@ne\dimexpr\wd\z@/2\relax\else\dimexpr\wd\@ne/2\relax\fi
  %
  \ifstrequal{#1}{l}{%
    \rlap{\iftoggle{Alt@before}{\usebox\z@}{\usebox\@ne}}%
    \copy\tw@
    \box\tw@
  }{%
    \ifstrequal{#1}{c}{%
      \copy\tw@
      \clap{\iftoggle{Alt@before}{\usebox\z@}{\usebox\@ne}}%
      \box\tw@
    }{%
      \ifstrequal{#1}{r}{%
        \copy\tw@
        \box\tw@
        \llap{\iftoggle{Alt@before}{\usebox\z@}{\usebox\@ne}}%
      }{%
      }%
    }%
  }%
}


%% fin alt


%%% Permet de choisir quels overlays on garde pour la version handout ou article, au lieu d'avoir tout.
%%% https://tex.stackexchange.com/a/455459
\newif\ifOnBeamerModeTransition
\newcommand{\slideselection}[1]{#1}%
\newenvironment{handoutframeselect}[1][1-]{%
  \begingroup%
  \mode<handout|article>{%
    \xdef\beamer@actualcurrentmode{\beamer@currentmode}
    \gdef\beamer@currentmode{beamer}%
    \OnBeamerModeTransitiontrue%
    \renewcommand{\slideselection}[1]{#1}}%
}{%
  \ifOnBeamerModeTransition%
    \OnBeamerModeTransitionfalse%
    \xdef\beamer@currentmode{\beamer@actualcurrentmode}%
  \fi%
  \endgroup%
}

%%% https://tex.stackexchange.com/questions/6135/how-to-make-beamer-overlays-with-tikz-node#6155
%%% voir aussi https://tex.stackexchange.com/questions/99119/beamer-problematic-use-of-visible-and-only-in-combination-with-tikz-to-draw-a#99122
%%% https://tex.stackexchange.com/a/55849
\AtBeginDocument{
\@ifpackageloaded{tikz}{
\tikzset{
invisible/.style={opacity=0, text opacity=0},
visible on/.style={alt={#1{}{invisible}}},
alt/.code args={<#1>#2#3}{%
      \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
    },
onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} 
}}
}%else
{}
}