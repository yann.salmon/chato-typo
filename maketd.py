#!/usr/bin/env python3

import subprocess
import sys
import os
import re

def compiler(nf, lversions, parallel = False) :
   """Compile un document de nom nf, avec les suffixes de jobname de lsuff, éventuellement en parallèle."""
   basedir = os.path.dirname(nf)
   builddir = os.path.join(basedir, "_build")
   basejobname = os.path.splitext(os.path.basename(nf))[0]
   if parallel :
      call = subprocess.Popen
   else :
      call = subprocess.call
   # https://stackoverflow.com/a/9745864
   processes = {}
   retVal = {}
   erreur = False
   for (suffixe, options, _) in lversions :
      processes[suffixe] = call('''TERM=dumb latexmk -pdflua -shell-escape -bibtex-cond1 -cd -silent -dvi- -jobname=''' +basejobname+suffixe +''' -auxdir=''' + builddir + " " + nf, shell=True, stdout=subprocess.PIPE,
                   bufsize=1, close_fds=True,
                   universal_newlines=True)
   if parallel :
      while len(processes) > 0 :
         for psuf in list(processes.keys()):
               p = processes[psuf]
               if p.poll() is not None: # process ended
                     p.stdout.close()
                     retVal[psuf] = p.returncode
                     processes.pop(psuf)
   else :
      for psuf in processes :
         retVal[psuf] = processes[psuf]
   for (suffixe, _, postproc) in lversions :
      if retVal[suffixe] != 0 :
         erreur = True
         with open(os.path.join(builddir, basejobname+suffixe+".log")) as f :
            for l in f :
               if l.startswith("! ") :
                  print("Erreur pour", suffixe, ":", l[2:], end="")
                  print(f.readline(), end="")
                  break
      try :
         #os.rename(os.path.join(builddir, basejobname+suffixe+".pdf"), os.path.join(basedir, basejobname+suffixe+".pdf"))
         if postproc is not None :
            postproc(os.path.join(basedir, basejobname+suffixe+".pdf"))
      except :
         print("Pas de PDF pour", suffixe)
   return 1 if erreur else 0

def booklet(nf) :
   #subprocess.call(["pdfbook2", nf, "--paper", "a3paper", "--no-crop"])
   pass



def clean(nf, suff) :
   subprocess.call(["latexmk", "-cd", "-c",  "-jobname="+os.path.splitext(os.path.basename(nf))[0]+suff, nf])

if sys.argv[1].endswith(".tex") :
   nf = sys.argv[1]
else :
   nf = sys.argv[1]+".tex"

# On lit le début du fichier pour trouver le nom de la classe LaTeX qu'il utilise
donnees = ""
with open(nf) as f :
   for l in f :
      if not l.strip().startswith("%") :
         donnees += l
      if len(donnees) > 300 :
         break
donnees = donnees[donnees.find(r"\documentclass")+len(r"\documentclass"):].strip()
optionsClasse = []
if donnees[0] == "[" :
   empilement = 0
   i = 1
   derniereOption=1
   while empilement != 0 or donnees[i] != "]" :
      if donnees[i] == "{" :
         empilement += 1
      elif donnees[i] == "}" :
         empilement -=1
      elif empilement == 0 and donnees[i] == "," :
         option = donnees[derniereOption:i].strip()
         if option != "" :
            optionsClasse.append(option)
         derniereOption = i+1
      i += 1
   option = donnees[derniereOption:i].strip()
   if option != "" :
      optionsClasse.append(option)
   donnees = donnees[i+1:].strip()

classe = donnees[1:donnees.find("}")]

# En fonction de la classe, on a différents suffixes de compilation pour jobname
if classe == "beamerarticle"  :
   modes = [("-BEAMER", "", None), ("-PRINT", "", booklet)]
elif classe == "chato-beamer" :
   if "avecarticle" in optionsClasse :
      modes = [("-BEAMER", "", None), ("-ARTICLE", "", booklet), ("-TRANS-2ON1", "", booklet)]
   else :
      modes = [("-BEAMER", "", None), ("-HANDOUT-2ON1", "", booklet), ("-TRANS-2ON1", "", booklet)]
elif classe == "chato-exo" or classe == "chato-exo2" :
   modes = [("-ENONCE", "", booklet), ("-SOLUTION", "", booklet), ("-COMPLET", "", booklet)]
else :
   raise ValueError("Classe {} non reconnue.".format(classe))



exit(compiler(nf, modes, len(sys.argv)>2 and sys.argv[2]=="--parallel"))

