\NeedsTeXFormat {LaTeX2e}
\ProvidesClass {chato-exo}[2024-04-16 v0.6]

% l3keys2e fait partie de LaTeX base depuis juin 2022
% l3keys2e charge expl3
\@ifl@t@r\fmtversion{2022-06-01}{}{\RequirePackage{l3keys2e, xparse}}
\ExplSyntaxOn

\sys_if_engine_luatex:F
 {\sys_if_engine_xetex:F
  {\PackageError{chato-exo}{Ne~fonctionne~qu'avec~XeLaTeX~ou~LuaLaTeX}{}}}

% mode de compilation : enonce, solution, complet = les deux
% C'est une token-list plutôt qu'une string parce que sinon la comparaison des modes
% avec \clist_if_in ne fonctionne pas (https://tex.stackexchange.com/q/629998).
% On définira \g__chato_exo_mode_str quand on aura la valeur définitive.
\tl_new:N \g__chato_exo_mode_tl

% permet de charger le paquet avec [mode=enonce] ou solution ou complet
% et de définir les éléments de l'en-tête : niveau, nature, titre, etablissement
\keys_define:nn { chato-exo }
  {
     mode   .choices:nn = {enonce, solution, complet} {\tl_gset:Nx \g__chato_exo_mode_tl {\tl_use:N \l_keys_choice_tl}},
    niveau .tl_gset:N = \g__chato_niveau_tl,
    nature .tl_gset:N = \g__chato_nature_tl,
    titre  .tl_gset:N = \g__chato_titre_tl,
    etab   .tl_gset:N = \g__chato_etab_tl,
    mode    .value_required:n = {true},
    niveau  .value_required:n = {true},
    nature  .value_required:n = {true},
    titre   .value_required:n = {true},
    etab   .initial:n = {Lycée~Chateaubriand},
    lang    .tl_gset:N = \g__chato_lang_tl,
    lang   .initial:n = {french},
    unknown .code:n           =
     \PackageWarning{chato-exo}{Unknown~ option~ `\l_keys_key_str'}
  }

% \ProcessKeyOptions fait partie de LaTeX base depuis juin 2022.
% Par ailleurs on n'a pas avec lui de problème d'espaces,
% donc plus besoin de {} sur le titre/nature,
% donc plus besoin du hack ci-après.
\@ifl@t@r\fmtversion{2022-06-01}
  {\ProcessKeyOptions}
  {\ProcessKeysOptions { chato-exo }}

% On supprime la liste des options de la classe car si babel la reçoit,
% il va déclencher une erreur en raison de la présence de {} dans notre nature ou titre.
% cf https://tex.stackexchange.com/questions/479552
\clist_clear:N \@classoptionslist

% permet de changer le mode selon que jobname contient -ENONCE, -SOLUTION ou -COMPLET
 \str_if_in:NnTF {\c_sys_jobname_str} {-ENONCE} {\tl_gset:Nn \g__chato_exo_mode_tl {enonce}}
{\str_if_in:NnTF {\c_sys_jobname_str} {-SOLUTION} {\tl_gset:Nn \g__chato_exo_mode_tl {solution}}
{\str_if_in:NnT  {\c_sys_jobname_str} {-COMPLET} {\tl_gset:Nn \g__chato_exo_mode_tl {complet}}
}}

\str_const:Nx \g__chato_exo_mode_str {\tl_to_str:N \g__chato_exo_mode_tl}

\LoadClass[10pt, a4paper, \g__chato_lang_tl]{article}
\RequirePackage[\g__chato_lang_tl]{babel}
\exp_args:No \str_case:Nn {\tl_to_str:N \g__chato_lang_tl}
{
  {french}
    {\frenchbsetup{og=«, fg=»}}
}

\RequirePackage{csquotes}
\RequirePackage[citestyle=alphabetic,bibstyle=alphabetic]{biblatex}
\RequirePackage{amsmath,stmaryrd,amssymb,fancybox,fancyhdr,lastpage,mdwlist}

% contiendra COMPLET ou SOLUTION dans le mode idoine
\str_new:N \g__chato_exo_mode_titre_str

%%% GESTION DES LABELS EN MODE SOLUTION
% Plutôt que d'ignorer complètement le contenu des questions, on va le typographier dans
% une vbox qu'on va jeter ensuite. L'intérêt est que si des labels sont définis,
% par exemple pour des équations, on pourra y faire référence quand même.
% Pour que ça marche, il faut extraire les labels de la boite.
% Pour cela, on modifie localement \label
% pour qu'il mette les informations voulues dans une liste \g__chato_exo_labels_cl, qu'on 
% traite après avoir refermé la boite en appliquant sur la liste une fonction qui imite ce que 
% fait originellement \label (adaptation de https://tex.stackexchange.com/a/273480).

% Selon que amsmath ou hyperref sont chargés, il y des modifs :
%   - manipuler \ltx@label au lieu de \label (amsmath)
%   - stocker et utiliser plus d'informations pour chaque label (hyperref)
% Il faut donc charger ces paquets avant, pour que le comportement approprié puisse être pris.

% Liste des label au sein de l'environnement
\clist_new:N \g__chato_exo_labels_cl
% Boite poubelle pour typographier l'environnement
\box_new:N \l__chato_exo_dummy_box

  
% Si hyperref est chargé, il y a plus d'informations à stocker et ressortir
% \cs__chato_exo_qlabel:n sera le \label ou \ltx@label dans l'environnement : 
%     il ajoute les infos de label (3 ou 5) dans la liste.
% \cs__chato_exo_dolabels prend 3 ou 5 paramètres et fait ce que ferait \label ordinairement.
\@ifpackageloaded{hyperref}
  {
    \cs_new:Npn\cs__chato_exo_dolabels#1#2#3#4#5{\@bsphack \begingroup \def \label@name {#1}\label@hook \protected@write \@auxout {}{\string \newlabel {#1}{{#2}{#3}{#4}{#5}{}}}\endgroup \@esphack}
    \cs_new:Nn\cs__chato_exo_qlabel:n{\clist_gput_right:Nx \g__chato_exo_labels_cl { {{#1}{\@currentlabel}{\thepage}{\@currentlabelname}{\@currentHref}} } }
  }
  {
    \cs_new:Npn\cs__chato_exo_dolabels#1#2#3{\@bsphack \protected@write \@auxout {}{\string \newlabel {#1}{{#2}{#3}}}\@esphack}
    \cs_new:Nn\cs__chato_exo_qlabel:n{\clist_gput_right:Nx \g__chato_exo_labels_cl { {{#1}{\@currentlabel}{\thepage}} } }
  }

% Pour savoir si on est en train de cacher un environnement, on compte combien sont en cours.
% L'idée est de ne pas cacher dans quelque chose de caché, car ça mettrait le bazar
% avec la liste des labels.
\int_new:N \g__chato_exo_silent_en_cours_int

% Ceci est à mettre au début de l'environnement qu'on veut masquer
\tl_const:Nn \g__chato_exo_begin_silent_env_tl
  {\int_compare:nNnT{\g__chato_exo_silent_en_cours_int}={0}{
    \group_begin:
    \cs_set_eq:NN\label\cs__chato_exo_qlabel:n
    %% Si amsmath est chargé, tout environnement comme equation redéfinit label pour faire des trucs, et ensuite appeler \ltx@label : on doit donc aussi remplacer cette commande.
    %% https://tex.stackexchange.com/a/655523
    \@ifpackageloaded{amsmath}{\cs_set_eq:NN\ltx@label\cs__chato_exo_qlabel:n}{}
    %% Cette commande est appelée quand on démarre un figure notamment.
    %% Cette définition, inspirée du mode H du paquet float, désactive tous les traitements spécifiques des floats mais définit \@captype afin que le caption soit correctement géré.
    \cs_set_nopar:Npn\@xfloat #1[#2]{\cs_set_eq:cN {end#1}\relax\cs_set_nopar:Npn \@captype {#1}}
     \vbox_set:Nw \l__chato_exo_dummy_box
   }
   \int_gincr:N\g__chato_exo_silent_en_cours_int}

% Ceci est à mettre à la fin   
\tl_const:Nn \g__chato_exo_end_silent_env_tl
  {\int_gdecr:N\g__chato_exo_silent_en_cours_int
   \int_compare:nNnT{\g__chato_exo_silent_en_cours_int}={0}{
     \vbox_set_end:\group_end:
     \clist_map_inline:Nn \g__chato_exo_labels_cl {\cs__chato_exo_dolabels #1}
     \clist_gclear:N \g__chato_exo_labels_cl
   }}

%%% FIN GESTION DES LABELS EN MODE SOLUTION

%%% AFFICHAGE DES QUESTIONS ET SOLUTIONS
\RequirePackage{amsthm, thmtools, etoolbox, enumitem}
\RequirePackage{chato-comment}

% Pour indenter le contenu du "théorème" (contenu des exos) par rapport au numéro
%% https://tex.stackexchange.com/a/106582
\patchcmd\@thm
  {\trivlist}
  {\list{}{\leftmargin3.2em\itemindent-15em}}
  {}{}
\newcommand{\xdeclaretheorem}[2][]{%
  \declaretheorem[#1]{#2}%
  \expandafter\patchcmd\csname thmt@original@end#2\endcsname
    {\endtrivlist}{\endlist}{}{}%
}
% Pour gérer le fait qu'on veut aller à la ligne après le titre d'un exo, mais pas s'il n'y a pas de titre.
% Le \leavevmode est là pour forcer le passage à la ligne quand l'énoncé débute par une énumération (cf. doc de amsthm, section 4.3.1).
\cs_new:Nn\cs__chato_exo_aLaLigne:{\nopagebreak\leavevmode\nopagebreak\par\nopagebreak}
\cs_new_eq:NN\cs__chato_exo_finLigne:\prg_do_nothing:

% Ne fonctionne pas chez Amélie…
%\str_case:VnF \g__chato_exo_mode_str
\exp_args:No \str_case:nnF {\g__chato_exo_mode_str}
  { % debut case
    {enonce}{
      % \str_log:n n'existe pas chez Amélie
      %\str_log:n {chato-exo~:~Mode~énoncé}
      \declaretheoremstyle[
        spaceabove=2ex,
        notebraces={}{\cs_gset_eq:NN\cs__chato_exo_finLigne:\cs__chato_exo_aLaLigne:},
        notefont=\normalfont\sffamily,
        headfont=\normalfont\bfseries,
        bodyfont=\normalfont,
        headpunct={},
        headformat={\hskip-.4em\makebox[0pt][r]{\NUMBER.}\NOTE},
        postheadhook={\cs__chato_exo_finLigne:\cs_gset_eq:NN\cs__chato_exo_finLigne:\prg_do_nothing:},
        preheadhook={\pagebreak[0]},
      ]{styleQuestion}
      \xdeclaretheorem[style=styleQuestion, title={\hskip-.9ex.}]{question}
      
      \declaretheoremstyle[
              spacebelow=1ex,
              notebraces={}{\cs_gset_eq:NN\cs__chato_exo_finLigne:\cs__chato_exo_aLaLigne:},
              notefont=\normalfont\sffamily,
              headfont=\normalfont\bfseries,
              bodyfont=\normalfont,
              headpunct={},
              headformat={\hskip-.4em\makebox[0pt][r]{Test.}\NOTE},
              postheadhook={\cs__chato_exo_finLigne:\cs_gset_eq:NN\cs__chato_exo_finLigne:\prg_do_nothing:},
            ]{styleTest}
            \xdeclaretheorem[numbered=no, style=styleTest]{test}
      
      \managecomment{solution}
      \managecomment{questionPTP}
      \managecomment{testPTP}
    }
    {solution}{
      %\str_log:n {chato-exo~:~Mode~solution}
      \str_set:Nn \g__chato_exo_mode_titre_str {SOLUTION}
      % Ceci permet de compter les questions, alors même qu'on ne les affiche pas.
      % Ce compteur sera celui utilisé pour produire le numéro de la solution.
      % C'est ce dispositif qui permet une numérotation correcte des solutions lorsque 
      % certaines questions ne sont pas corrigées.
      \newcounter{question}
      \NewDocumentEnvironment{question}{O{}}
      {\refstepcounter{question}
       \g__chato_exo_begin_silent_env_tl}
      {\g__chato_exo_end_silent_env_tl}
      
      \NewDocumentEnvironment{test}{O{}}
      {\g__chato_exo_begin_silent_env_tl}
      {\g__chato_exo_end_silent_env_tl}
      
      \declaretheoremstyle[
        spaceabove=2ex,
        notebraces={}{\cs_gset_eq:NN\cs__chato_exo_finLigne:\cs__chato_exo_aLaLigne:},
        notefont=\normalfont\sffamily,
        headfont=\normalfont\bfseries,
        bodyfont=\normalfont,
        headpunct={},
        headformat={\hskip-.4em\makebox[0pt][r]{\thequestion.}\NOTE},
        postheadhook={\cs__chato_exo_finLigne:\cs_gset_eq:NN\cs__chato_exo_finLigne:\prg_do_nothing:},
        preheadhook={\pagebreak[0]},
      ]{styleSolutionQuestion}
      % Techniquement, le théorème solution n'est pas numéroté.
      % Son affichage utilise le compteur question, qui est indépendant de lui.
      \xdeclaretheorem[numbered=no, style=styleSolutionQuestion]{solution}
      
      \declaretheoremstyle[
                  spaceabove=2ex,
                  notebraces={}{\cs_gset_eq:NN\cs__chato_exo_finLigne:\cs__chato_exo_aLaLigne:},
                  notefont=\normalfont\sffamily,
                  headfont=\normalfont\bfseries,
                  bodyfont=\normalfont,
                  headpunct={},
                  headformat={\hskip-.4em\makebox[0pt][r]{\NUMBER.}\NOTE},
                  postheadhook={\cs__chato_exo_finLigne:\cs_gset_eq:NN\cs__chato_exo_finLigne:\prg_do_nothing:},
                  preheadhook={\pagebreak[0]},
                ]{styleQuestion}
                \xdeclaretheorem[style=styleQuestion, title={\hskip-.9ex.}]{questionPTP}
                
            \declaretheoremstyle[
                    spacebelow=1ex,
                    notebraces={}{\cs_gset_eq:NN\cs__chato_exo_finLigne:\cs__chato_exo_aLaLigne:},
                    notefont=\normalfont\sffamily,
                    headfont=\normalfont\bfseries,
                    bodyfont=\normalfont,
                    headpunct={},
                    headformat={\hskip-.4em\makebox[0pt][r]{Test.}\NOTE},
                    postheadhook={\cs__chato_exo_finLigne:\cs_gset_eq:NN\cs__chato_exo_finLigne:\prg_do_nothing:},
                  ]{styleTest}
                  \xdeclaretheorem[numbered=no, style=styleTest]{testPTP}
    }
    {complet}{
      %\str_log:n {chato-exo~:~Mode~complet}
      \str_set:Nn \g__chato_exo_mode_titre_str {COMPLET}
      \declaretheoremstyle[
        spaceabove=2ex,
        notebraces={}{\cs_gset_eq:NN\cs__chato_exo_finLigne:\cs__chato_exo_aLaLigne:},
        notefont=\normalfont\sffamily,
        headfont=\normalfont\bfseries,
        bodyfont=\normalfont,
        headpunct={},
        headformat={\hskip-.4em\makebox[0pt][r]{\NUMBER.}\NOTE},
        postheadhook={\cs__chato_exo_finLigne:\cs_gset_eq:NN\cs__chato_exo_finLigne:\prg_do_nothing:},
        preheadhook={\pagebreak[0]},
      ]{styleQuestion}
      \xdeclaretheorem[style=styleQuestion, title={\hskip-.9ex.}]{question}
      
      \declaretheoremstyle[
              spacebelow=1ex,
              notebraces={}{\cs_gset_eq:NN\cs__chato_exo_finLigne:\cs__chato_exo_aLaLigne:},
              notefont=\normalfont\sffamily,
              headfont=\normalfont\bfseries,
              bodyfont=\normalfont,
              headpunct={},
              headformat={\hskip-.4em\makebox[0pt][r]{Test.}\NOTE},
              postheadhook={\cs__chato_exo_finLigne:\cs_gset_eq:NN\cs__chato_exo_finLigne:\prg_do_nothing:},
            ]{styleTest}
            \xdeclaretheorem[numbered=no, style=styleTest]{test}
      
            \xdeclaretheorem[numbered=no, style=styleTest]{testPTP}
      
      \declaretheoremstyle[
        notebraces={}{\cs_gset_eq:NN\cs__chato_exo_finLigne:\cs__chato_exo_aLaLigne:},
        notefont=\normalfont\sffamily,
        headfont=\normalfont\bfseries,
        bodyfont=\normalfont,
        headpunct={},
        headformat={\hskip-.4em\makebox[0pt][r]{Solution.}\NOTE},
        postheadhook={\cs__chato_exo_finLigne:\cs_gset_eq:NN\cs__chato_exo_finLigne:\prg_do_nothing:},
      ]{styleSolution}
      \xdeclaretheorem[numbered=no, style=styleSolution]{solution}
      
      \xdeclaretheorem[style=styleQuestion, title={\hskip-.9ex.}]{questionPTP}
    }
  } % fin case
  { %% aucun cas trouvé
    \PackageError{chato-exo}{Mode~non~spécifié~ou~invalide~:~\g__chato_exo_mode_str}{Vous~devez~charger~le~paquet~avec~mode=enonce~ou~solution~ou~complet,~ou~bien~utiliser~un~jobname~qui~contient~-ENONCE,~-SOLUTION~ou~-COMPLET.}
  }

\newlist{ssquest}{enumerate}{1}
\setlist[ssquest]{label=(\alph*), ref={\thequestion.(\alph*)}}
%%% FIN AFFICHAGE DES QUESTIONS ET SOLUTIONS


%%% GESTION DES ENVIRONNEMENTS À MODE
% Détecte si le mode en cours fait partie de la liste de modes passée en paramètre (convention : oui si la liste est vide).
\prg_new_conditional:Nnn \if__chato_exo_mode_active:n {T,F,TF} {
  \clist_if_empty:nTF {#1}
    {\prg_return_true:}
    {\clist_if_in:nVTF {#1} \g__chato_exo_mode_tl
      {\prg_return_true:}
      {\prg_return_false:}}
}

\NewDocumentEnvironment{onlyenv}{D<>{}}
  {\if__chato_exo_mode_active:nF{#1}{\g__chato_exo_begin_silent_env_tl}}
  {\if__chato_exo_mode_active:nF{#1}{\g__chato_exo_end_silent_env_tl}}

\NewDocumentEnvironment{remarque}{D<>{}}
  {\if__chato_exo_mode_active:nTF{#1}
    {\par\textbf{Remarque~:}}
    {\g__chato_exo_begin_silent_env_tl}
  }
  {\if__chato_exo_mode_active:nF{#1}{\g__chato_exo_end_silent_env_tl}}
  
\RequirePackage{multicol}
\NewDocumentEnvironment{colonnes}{mD<>{}}
  {\if__chato_exo_mode_active:nT{#2}{\begin{multicols}{#1}}}
  {\if__chato_exo_mode_active:nT{#2}{\end{multicols}}}
  
\NewDocumentCommand{\colonnebreak}{D<>{}}{
  \if__chato_exo_mode_active:nT{#1}{\columnbreak}
}

\NewCommandCopy\chato__exo_pagebreak\pagebreak
\RenewDocumentCommand{\pagebreak}{O{4}D<>{}}{
  \if__chato_exo_mode_active:nT{#2}{\chato__exo_pagebreak[#1]}
}

\RequirePackage{mdframed}
\NewDocumentCommand{\cadreReponse}{O{4cm}}{
  \if__chato_exo_mode_active:nT {enonce}
  {\begin{mdframed}[linewidth=1pt]\vspace{#1}\end{mdframed}}}

%%% FIN GESTION DES ENVIRONNEMENTS À MODE

%\topmargin-1.5cm
%\headheight15pt
%\textheight24.9cm
%\textwidth17cm
%\oddsidemargin-1cm
%\evensidemargin0cm

\RequirePackage{geometry}
\geometry{a4paper, top=1cm, includehead, head=0.53cm, bottom=1cm, includefoot, left=1.5cm, right=1.5cm}
\NewDocumentCommand{\Paysage}{D<>{}}{
\if__chato_exo_mode_active:nT {#1} {\geometry{landscape}}
}

\RequirePackage{chato-listing, chato-notations, chato-tikz}


\AtBeginDocument{
\pagestyle{fancy}

\lhead{{{\textit{\g__chato_nature_tl}}}~---~{{\g__chato_niveau_tl}}}
\chead{\g__chato_exo_mode_titre_str}
\rhead{\bfseries{\large \g__chato_titre_tl}}
\cfoot{}
\lfoot{\g__chato_etab_tl}
\rfoot{\thepage/\pageref{LastPage}}

\renewcommand{\footrulewidth}{0.4pt}
}

\ExplSyntaxOff