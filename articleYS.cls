\NeedsTeXFormat {LaTeX2e}
\ProvidesClass {articleYS}[2022-12-23 v0.1] 

\ExplSyntaxOn

\ProvideDocumentCommand{\nature}{m}{\str_const:Nn \g__chato_beamer_nature {#1}}
\ProvideDocumentCommand{\niveau}{m}{\str_const:Nn \g__chato_beamer_niveau {#1}}
\AtBeginDocument{
 \str_if_exist:NF \g__chato_beamer_natniv {
   \str_new:N \g__chato_beamer_natniv
    \str_if_exist:NT \g__chato_beamer_nature {
      \str_gset_eq:NN \g__chato_beamer_natniv \g__chato_beamer_nature
      \str_if_exist:NT \g__chato_beamer_niveau {
        \str_gput_right:Nn \g__chato_beamer_natniv {~---~}
      }
    }
    \str_if_exist:NT \g__chato_beamer_niveau {
       \str_gput_right:NV \g__chato_beamer_natniv \g__chato_beamer_niveau
    }
  }
}

\tl_if_exist:NF \g__chato_beamer_lang {\tl_gset:Nn \g__chato_beamer_lang {french}}
\bool_if_exist:NF \g__chato_beamer_complet_mode {\bool_new:N \g__chato_beamer_complet_mode}
\PassOptionsToClass{\g__chato_beamer_lang, 10pt, a4paper, DIV=12}{scrartcl}

\LoadClass{scrartcl}

\RequirePackage[\g__chato_beamer_lang]{babel}
\RequirePackage{csquotes}

%\RequirePackage{enumitem}

\RequirePackage{tabularray}
\UseTblrLibrary{counter}

\addtokomafont{disposition}{\rmfamily}
\setkomafont{descriptionlabel}{\bfseries}

\RequirePackage{geometry}
\geometry{top=16.5mm,bottom=25mm,left=10mm,right=10mm}
\DeclareOption{margedroite}{\geometry{top=16.5mm,bottom=25mm,left=7.5mm,right=45mm}}

\ProcessOptions

%% Les espaces avant et après le titre sont trop importants.
%% On patche \@maketitle pour ajouter des espaces négatifs au début et à la fin.
\RequirePackage{xpatch}
\xpretocmd{\@maketitle}{\str_if_empty:NTF\g__chato_beamer_natniv{\vspace*{-6em}}{\vspace*{-5em}}}{}{}
\xapptocmd{\@maketitle}{\vspace*{-2em}}{}{}

\RequirePackage{microtype}

\RequirePackage{lastpage}
\renewcommand\pagemark{{\usekomafont{pagenumber}\thepage\ sur\ \pageref{LastPage}}}

\RequirePackage[headsepline, footsepline, plainfootsepline]{scrlayer-scrpage}


\AtBeginDocument{
  \str_set:NV\g__chato_article_title{\@title}
  \str_remove_all:Nn\g__chato_article_title{\\}
  \ohead{\g__chato_article_title}
  \str_if_exist:NT\g__chato_beamer_natniv{\ihead*{\g__chato_beamer_natniv}}
  \bool_if:nT{\g__chato_beamer_complet_mode}{\chead*{\normalfont COMPLET}}
}

\clearpairofpagestyles
\cfoot*{\pagemark}

\RequirePackage[hidelinks]{hyperref}

\RequirePackage{chato-listing, chato-notations}