#!/usr/bin/env python3

import re
import sys

p = re.compile(r"(?:<((?:[0-9.,\-|]|[a-z]+@|(?:article|handout|trans|beamer|second):?|\s+)+)>)|(?:\\pause\[((?:[0-9.])+)\])")

def isnumber(s) :
    try :
        return float(s) != 0.0
    except ValueError :
        return False

LSEP = ("|", ":", "@", ",", "-")

def ressplit(s, lsep, res) :
    if len(lsep) == 0 :
        if isnumber(s) :
            res |= {float(s)}
    else :
        sep, *lsep = lsep
        for x in s.split(sep) :
            ressplit(x, lsep, res)

def extract(s):
    res = set()
    for specTuple in p.findall(s) :
        for spec in specTuple :
            ressplit(spec, LSEP, res)
    return res

def subsplit(s, lsep, newi) :
    if len(lsep) == 0 :
        return str(newi[float(s)]) if isnumber(s) else s
    sep, *lsep = lsep
    return sep.join(subsplit(x, lsep, newi) for x in s.split(sep))

def sub(s, newi):
    m = p.search(s)
    if m is None :
        return s
    else :
        return s[:m.start(m.lastindex)] + subsplit(m.group(m.lastindex), LSEP, newi) + sub(s[m.end(m.lastindex):], newi)

def do_file(fn, start=0, remove=None) :
    if remove is None : remove = set()
    ls = open(fn, "r+").readlines()[start:]
    for k in range(len(ls)) :
        if r"\end{frame}" in ls[k] :
            ls = ls[:k]
            break
    xs = set.union(*[extract(l) for l in ls])
    # some integer overlays may not be referred to (especially 1), so we add them
    xs |= set(range(1, int(max(xs))))
    # remove unwanted overlays
    xs -= remove
    newi = dict(zip(sorted(xs), range(1, len(xs)+1)))
    for l in ls:
        sys.stdout.write(sub(l, newi))

def test() :
    assert extract(r"(fun v -> \uncover<10.1->{if pred.(v) = -1 then} \action<handout:0|10.2,13.5-|alert@13.5-13.7,8|article|beamer:1>{(}Queue.push v front \uncover<11.2->{\alert<11.3>{;} pred.(v) <- s\alert<11>{)}})") == {1.0, 8.0, 10.1, 10.2, 11.0, 11.2, 11.3, 13.5, 13.7}

if __name__ == "__main__" :
    if len(sys.argv) > 1 :
        if not sys.argv[1].endswith(".tex") :
            fichier = sys.argv[1]+".tex"
        else :
            fichier = sys.argv[1]
        do_file(fichier, int(sys.argv[2]) if len(sys.argv) > 2 else 0, {int(x) for x in sys.argv[3].split(",")} if len(sys.argv)>3 else set())
    else :
        print("""Give filename as 1st parameter and, optionally, starting line number as 2nd, and optionally, list of integer overlays to remove as 3rd.
        Program will stop when encountering \\end{frame}.
        Based on https://tex.stackexchange.com/a/139498""")